let startObj = document.getElementById("start");
let stopObj = document.getElementById("stop");
const video = document.querySelector("video");
let recorder, stream,audio;
 var count = 0;
let chunks = [];
async function startRecording() {
  stream = await navigator.mediaDevices.getDisplayMedia({
    video: { mediaSource: "screen" },
	audio: true
  });

const op={
mimeType:'video/webm;codecs=vp9'
}
  recorder = new MediaRecorder(stream,op);
  let lis = [];
  recorder.ondataavailable = e => lis.push(e.data);
  recorder.onstop = e => {
    let completeBlob = new Blob(lis, { type: 'video/webm;codecs=vp9' });
    video.src = window.URL.createObjectURL(completeBlob);

  };

  recorder.start();
}

startObj.addEventListener("click", () => {
  startObj.setAttribute("disabled", true);
  stopObj.removeAttribute("disabled");

  startRecording();

});

stopObj.addEventListener("click", () => {
  count=0;
  stopObj.setAttribute("disabled", true);
  startObj.removeAttribute("disabled");
  recorder.stop();
  stream.getVideoTracks()[0].stop();
console.log(video)
});
